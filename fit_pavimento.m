% fit del pavimento
frame= load('SS190/acqe1_075118.mat');
Z=double(frame.rawdata.Z');
figure
imshow(Z,[]); colormap jet

%%
[col,row]=ginput(16);
col=round(col);
row=round(row);
A=[row,col,ones(16,1)];

for ii=1:16
    b(ii)=Z(row(ii),col(ii));
end

b=b';

param=A\b;

save('Zfit-param-SS190', 'param');

[COL,ROW] = meshgrid(1:size(Z,2),1:size(Z,1));

Zpavimento=COL*param(2)+ROW*param(1)+param(3);

figure
imshow(Zpavimento,[]); colormap jet

save('Zpavimento-SS190', 'Zpavimento');

figure
imshow(Zpavimento-Z,[]); colormap jet
impixelinfo
