function filedata = loadSequence(folder, pattern)
%loadSequence loads a sequence of .mat files from a folder, matching
%the given regex pattern. It returns a cell array {filename, structured data}.
%   %folder is the absolute or relative path to parse
%   %pattern is a regex to match each file with

files=dir(folder);
numFiles=length(files)-2;
if numFiles > 0
    files=files(3:length(files)); %remove '.' and '..
else
    errordlg('The folder is empty.','Empty folder','modal')
    return
end

%extract a column cell array with filenames (excluding foldernames)
files=struct2cell(files);
[~,indices]=find(~cell2mat(files(4,:))); % it is a directory (isdir=1)

% order alphabetically and extract the filename only
[~,ind]=sort(files(1,indices));
files=files(1,ind)';

%if there are no .mat files or they're not the only content, error
% ismat = cell2mat(strfind(lower(files), '.mat')) ~= 0;
% if any(~ismat) || isempty(ismat) || numFrame > length(ismat)
%     errordlg('The folder must contain .mat files only.', 'Improper content', 'modal')
%     return;
% end

% filter only mat files by pattern
matfile = '\.mat$';
pattern = [pattern matfile];
% include zero-length matches
out = regexpi(files,pattern,'end','once');
matches = zeros(size(out));
for i=1:length(out)
    matches(i) = ~isempty(out{i});
end       

files = files(matches ~= 0);
numFiles = length(files);
if numFiles == 0
    errordlg('No .mat files were found matching the given pattern', 'Files not found','modal');
    filedata = [];
    return;
end
% add a field containing the whole path
for ii=1:numFiles
    if (ismac || isunix) %mac or unix platform
        files{ii,2}=[folder '/' files{ii}];
    else %windows platform
        files{ii,2}=[folder '\' files{ii}];
    end
end

filedata = cell(numFiles, 2);
filedata(:,1) = files(:,2);
for ii=1:numFiles
    filedata{ii,2} = load(files{ii,2},'-mat');
end

end

