function [curve_cell, labels] = make_curve_cell( trackdata_struct, t0, tf )
%MAKE_CURVE_CELL(trackdata_struct, t0, tf) Creates a trajectory cell array 
%where each cell i contains the trajectory in [x,y,z,t] of the pedestrian at 
%labels(i), according to the column indexing of trackdata_struct. 
%Works only if absent data is set to zero!
%   [curve_cell, labels] = MAKE_CURVE_CELL(trackdata, start, end) returns a
%   vote_neighbours-compliant structure curve_cell as well as the labels to
%   address the original indices. START and END arguments crop the curve between
%   the specified frame indices, not absolute times.
paths_x = {};
paths_y = {};
paths_z = {};
times = {};
% the i-th element in labels is the column index in trackdata_struct fields
% the i-th label in labels is associated with the i-th element in curves
labels = []; 

interval = t0:tf;

for i=1:size(trackdata_struct.xmetri, 2)
    indrows = find(trackdata_struct.xmetri(interval, i));
    offsets = indrows + t0 - 1;
    if ~isempty(indrows)
        labels = [labels, i];
        paths_x = [paths_x, trackdata_struct.xmetri(offsets, i)];
        paths_y = [paths_y, trackdata_struct.ymetri(offsets, i)];
        paths_z = [paths_z, trackdata_struct.zmetri(offsets, i)];
        times = [times, trackdata_struct.nframe_time(offsets, 1)];
    end
end

% store each curve in a cell as matrix <frames-by-3> s.t. each row is [x_t, y_t, z_t, t]
curves_len = size(paths_x, 2);
curve_cell = cell(curves_len, 1);
for i=1:curves_len
    curve_cell{i} = [paths_x{i}, paths_y{i}, paths_z{i}, times{i}];
end

end

