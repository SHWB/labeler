#LABELER

##Note per l'utente

###1. Aprire una sequenza

![](./doc/img/open.jpg)

Fare click su `File > Open Sequence...` per aprire una sequenza di acquisizioni in formato `.mat`. La cartella deve contenere **esclusivamente** i dati relativi alla sequenza che si vuole etichettare.

###2. Etichettatura

La modalità di default dell'interfaccia è quella di navigazione, in cui vengono mostrate, fotogramma per fotogramma, le etichette già presenti.
Si può navigare tra i frame anche utilizzando la tastiera: i tasti `A` (o `freccia sx`) e `D` (o `freccia dx`) consentono la navigazione rispettivamente verso il frame precedente e successivo.
Per procedere con l'etichettatura è necessario spuntare la check box `Edit Mode` in basso a sinistra. Al passaggio a questa modalità, viene aperta una finestra di conferma, che avvisa che **tutti i dati di etichettatura eventualmente già presenti a valle del fotogramma corrente verranno cancellati**. Inoltre, in questa modalità la navigazione libera è disabilitata. Questo comportamento del software è dovuto all'esigenza di produrre etichettature in cui le traiettorie siano il più possibile consistenti.

In `Edit Mode` è possibile aggiungere nuove etichette, nella maniera seguente.

* Cliccare con il tasto destro in corrispondenza del punto da etichettare (l'apice della testa del pedone che si intende etichettare). Si aprirà un menù contestuale che permetterà l'inserimento di un nuovo punto.
* Cliccare su `Insert new point...` invoca una finestra di dialogo che permette l'inserimento di un'etichetta **NUMERICA**. Il numero suggerito è incrementale ed è allineato col numero di pedoni già etichettati.
![](./doc/img/labeling.jpg)
* Una volta etichettati tutti i pedoni è possibile passare al frame successivo, il quale erediterà tutti i punti del fotogramma precedente. Sarà sufficiente allora spostare i punti in corrispondenza della nuova posizione del relativo pedone. La posizione viene aggiornata automaticamente durante il trascinamento. _Suggerimento: ai fini dello spostamento dei punti può risultare difficile cliccare l'esatta posizione del marker: è possibile sfruttare le etichette numeriche per un puntamento più agevole._
* Per **cancellare** un'etichetta, perché ad esempio un pedone scompare dalla scena, cliccare con il tasto destro sul punto (o sulla sua etichetta) che si desidera eliminare. Selezionare `Delete` dal menu contestuale che appare.

###3. Salvataggio e caricamento

![](./doc/img/export.jpg)

Etichettata la sequenza o una parte di essa, è possibile salvare il lavoro finora compiuto al menu `File > Export > Labeled points`.
Se si desidera caricare i punti salvati, cliccare su `File > Load labeled points...`.
Inoltre, è possibile utilizzare i dati di etichettatura per generare dati comparabili con quelli prodotti da `Pedoni.m`. Per fare questo, cliccare su `File > Export > CSV and .mat`. Verranno richiesti __due salvataggi diversi__, uno per il .mat ed uno per il .csv, come specificato nel titolo delle finestre di dialogo; se si desidera salvare soltanto uno dei due formati, è sufficiente annullare il salvataggio cui non si è interessati. L'esportazione può richiedere qualche minuto poiché coinvolge l'elaborazione delle immagini di profondità dell'intera sequenza. __ATTENZIONE: affinché l'esportazione termini con successo è necessario che nella corrente cartella lavoro sia presente il file `Zpavimento.mat`, contenente i dati di profondità del suolo.__
La corretta esecuzione di queste operazioni è notificata sulla console di MATLAB.
