function MHC = max_height_comparison(manual_file, DISPLAY)

close all; 

load('SS190\trackdata_SS190_11-02-27.mat');
load(manual_file);
load('param_xyz-SS190');
load('Zpavimento.mat');

%% write zpavimento plane equation in Hessian normal form
ground_coeff = [param_xyz(1:2); -1; param_xyz(3)];
ground_normalized_coeff = ground_coeff/norm(ground_coeff(1:3));
ground_normal = ground_normalized_coeff(1:3);
% therefore the Hessian normal equation of the plane is given by:
% ground_normal'*[x;y;z] = -ground_normalized_coeff(4);

%% compute new xyz 
pedestrians = size(manual_trackdata.I,2);

%replace zeros with NaNs so that it's possible to find the minima
mh_trackdata = trackdata;
I = mh_trackdata.I;
J = mh_trackdata.J;
Depth = zeros(size(mh_trackdata.Depth));

for col=1:size(trackdata.Depth, 2)
    not_zero_rows = find(trackdata.Depth(:,col));
    if ~isempty(not_zero_rows)
        heads = [mh_trackdata.xmetri(not_zero_rows, col)'; ...
            mh_trackdata.ymetri(not_zero_rows, col)';
            mh_trackdata.zmetri(not_zero_rows, col)'];
        heights = ground_normal'*heads + ground_normalized_coeff(4);
        %the person height is the maximum distance from the ground along the
        %path
        [auto_max_height, ~] = max(heights); 
        deltas = auto_max_height - heights;
        correct_heads = heads + ground_normal*deltas;
        Depth(not_zero_rows, col) = correct_heads(3,:)'*1000; 
    end
end

[mh_trackdata.xmetri, mh_trackdata.ymetri, mh_trackdata.zmetri] = mydepth2realworld(J,I,Depth);

[dis, vel] = compute_velocities(mh_trackdata);
mh_trackdata.nframe_dxmetri = dis.x;
mh_trackdata.nframe_dymetri = dis.y;
mh_trackdata.nframe_vxmetri = vel.x;
mh_trackdata.nframe_vymetri = vel.y;
mh_trackdata.nframe_vxymetri = vel.modulus;

%% do the same thing to the Depth field in manual_trackdata
mh_man_trackdata = manual_trackdata;

I = mh_man_trackdata.I;
J = mh_man_trackdata.J;
Depth = NaN(size(manual_trackdata.Depth));
for p=1:pedestrians
    not_nan_rows = find(~isnan(manual_trackdata.Depth(:, p))); 
    if ~isempty(not_nan_rows)
        heads = [mh_man_trackdata.xmetri(not_nan_rows, p)'; ...
            mh_man_trackdata.ymetri(not_nan_rows, p)';
            mh_man_trackdata.zmetri(not_nan_rows, p)'];
        heights = ground_normal'*heads + ground_normalized_coeff(4);
        %the person height is the maximum distance from the ground along the
        %path
        [man_max_height, ~] = max(heights); 
        deltas = man_max_height - heights;
        correct_heads = heads + ground_normal*deltas;
        Depth(not_nan_rows, p) = correct_heads(3,:)'*1000; 
    end
end

[mh_man_trackdata.xmetri, mh_man_trackdata.ymetri, mh_man_trackdata.zmetri] = mydepth2realworld(J,I,Depth);

[dis, vel] = compute_velocities(mh_man_trackdata);
mh_man_trackdata.nframe_dxmetri = dis.x;
mh_man_trackdata.nframe_dymetri = dis.y;
mh_man_trackdata.nframe_vxmetri = vel.x;
mh_man_trackdata.nframe_vymetri = vel.y;
mh_man_trackdata.nframe_vxymetri = vel.modulus;

%% show paths
birth = zeros(pedestrians, 1);
life_span = zeros(pedestrians, 1);

for i=1:pedestrians
   not_nans = ~isnan(manual_trackdata.I(:,i)); 
   birth(i) = find(not_nans,1);
   life_span(i) = sum(not_nans);
end

death = birth + life_span - 1;

votes = NaN(pedestrians,1);
closest = NaN(size(votes));
incidence_matrix = false(pedestrians, size(mh_trackdata.xmetri,2));
max_height_error = NaN(size(votes));
ninf_mh = NaN(size(votes));

looped_once = false;
for target=1:pedestrians
    
    if looped_once 
        disp('Press any key to plot the trajectories for the next pedestrian...');
        pause
    end

    frame_start = birth(target);
    frame_end = death(target);
    
    span = frame_start:frame_end;
    
    [~,td_start] = min(abs(trackdata.nframe_time(:,1) - frame_start));
    [~,td_end] = min(abs(trackdata.nframe_time(:,1) - frame_end));
    
    man_curve = [mh_man_trackdata.xmetri(span, target), ...
        mh_man_trackdata.ymetri(span, target),...
        mh_man_trackdata.zmetri(span, target),...
        mh_man_trackdata.nframe_time(span, 1)];
    
    [mh_curves, mh_labels] = make_curve_cell(mh_trackdata,td_start,td_end);
    
    votes(target) = vote_neighbours(man_curve, mh_curves);
    closest(target) = mh_labels(votes(target));
    incidence_matrix(target,closest(target)) = true;
    
    MHC.incidence_matrix = incidence_matrix;
    MHC.closest = closest;
    
    td_span = td_start:td_end;
    if DISPLAY
        % plot only xmetri and ymetri
        figure(1);
        plot(mh_man_trackdata.xmetri(span, target), mh_man_trackdata.ymetri(span, target), 'r-*');
        set(gca,'YDir','reverse');
        xlabel('X'); ylabel('Y');
        title(sprintf('Pedone %i - Plot X-Y', target));
        hold on;
        grid on; axis equal;
        axis auto;
        %axis([-1.5 1.5 -1.5 1.5]);
        for ii=1:size(trackdata.xmetri,2)
            %find which rows in the selected time window are not zero for the given
            %pedestrian ii
            indrows=find(trackdata.xmetri(td_span,ii));
            offsets = indrows + td_start - 1;
            %beware that indrows is indexed starting from 1, then it need add the
            %offset
            plot(mh_trackdata.xmetri(offsets, ii), ...
                mh_trackdata.ymetri(offsets, ii), 'g-o');
        end
        hold off;
           
        % plot manual pedestrian
        figure(2);
        %plot3(manual_trackdata.xmetri(span,target), manual_trackdata.ymetri(span,target), ...
        %    manual_trackdata.zmetri(span,target),'m-*');
        %hold on;
        plot3(mh_man_trackdata.xmetri(span, target), mh_man_trackdata.ymetri(span, target), ...
            mh_man_trackdata.zmetri(span, target), 'r-*');
        set(gca,'YDir','reverse');
        %set(gca,'ZDir','reverse');
        xlabel('asse X');ylabel('asse Y');zlabel('asse Z');
        title(sprintf('Pedone %i - Plot traiettorie', target));
        hold on;
        grid on; axis equal;
        axis auto;
        %axis([-1.5 1.5 -1.5 1.5 1 2.5]);
        for ii=1:size(trackdata.xmetri,2)
            indrows=find(trackdata.xmetri(td_span,ii));
            offsets = indrows + td_start - 1;
            plot3(mh_trackdata.xmetri(offsets,ii), ...
                mh_trackdata.ymetri(offsets, ii), ...
                mh_trackdata.zmetri(offsets, ii), 'g-o');
            % set(gca,'YDir','reverse','ZDir','reverse');xlabel('asse Y');ylabel('asse X');zlabel('asse Z');
            %pause
        end
        hold off;
        
        looped_once = true;
    end
end