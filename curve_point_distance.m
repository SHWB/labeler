function distance = curve_point_distance(point, curve)
%CURVE_POINT_DISTANCE(P,C) Evaluates the euclidean distance between the point 
%P and the curve C. 
%   d = CURVE_POINT_DISTANCE(P,C) returns the distance stated above, given that
%   P is a row vector of length N holding point coordinates, and C is an M-by-N 
%   matrix, where M is the number of points in the trajectory.

    %curve is a matrix whose each row is a point 
    %for each element in curve, subtract the vector point
    c2p_vectors = bsxfun(@minus, curve, point);
    %vectorized 2-norm
    p2p_distances = sqrt(diag(c2p_vectors * c2p_vectors.'));
    distance = min(p2p_distances);
   
end

