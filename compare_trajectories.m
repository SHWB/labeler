clear all; close all;  clc;

load('SS190\trackdata_SS190_11-02-27.mat');
load('manual-trackdata-SS190.mat')

pedestrians = size(manual_trackdata.I,2);

birth = zeros(pedestrians, 1);
life_span = zeros(pedestrians, 1);

for i=1:pedestrians
   not_nans = ~isnan(manual_trackdata.I(:,i)); 
   birth(i) = find(not_nans,1);
   life_span(i) = sum(not_nans);
end

death = birth + life_span - 1;


%% parameters

%the pedestrian in the manually labeled sequence to be compared against all
%the other pedestrians in the automatically labeled sequence
%target = 15;

looped_once = false;

for target=1:pedestrians
    
    if looped_once
        disp('Press any key to plot the trajectories for the next pedestrian...');
        pause
    end
    
    tot_seq_len = size(manual_trackdata.nframe_time, 1);
    frame_start = birth(target);
    frame_end = death(target);

    span = frame_start:frame_end;

    [~,td_start] = min(abs(trackdata.nframe_time(:,1) - frame_start));
    [~,td_end] = min(abs(trackdata.nframe_time(:,1) - frame_end));

    td_span = td_start:td_end;
    
    % plot only I and J
    figure(1);
    grid on; axis equal;
    plot(manual_trackdata.J(span, target), manual_trackdata.I(span, target), 'r-*');
    xlabel('J'); ylabel('I');
    set(gca,'YDir','reverse');
    xlim([0 320]); ylim([0 240]);
    title(sprintf('Pedone %i - Plot J-I', target));
    
    hold on;
    for ii=1:size(trackdata.xmetri,2)
        %find which rows in the selected time window are not zero for the given
        %pedestrian ii
        indrows=find(trackdata.I(td_span,ii));
        
        %beware that indrows is indexed starting from 1, then it need add the
        %offset
        plot(trackdata.J(indrows + td_start - 1, ii), ...
            trackdata.I(indrows + td_start - 1, ii),'b-o');
    end
    hold off;
    
    % plot only xmetri and ymetri
    figure(2);
    grid on; axis equal;
    plot(manual_trackdata.xmetri(span, target), manual_trackdata.ymetri(span, target), 'r-*');
    set(gca,'YDir','reverse');
    xlabel('X'); ylabel('Y'); 
    title(sprintf('Pedone %i - Plot X-Y', target));

    hold on;
    for ii=1:size(trackdata.xmetri,2)
       %find which rows in the selected time window are not zero for the given
       %pedestrian ii
       indrows=find(trackdata.xmetri(td_span,ii));

       %beware that indrows is indexed starting from 1, then it need add the
       %offset
       plot(trackdata.xmetri(indrows + td_start - 1, ii), ...
            trackdata.ymetri(indrows + td_start - 1, ii),'b-o');
    end
    hold off;

    % plot manual pedestrian
    figure(3);
    plot3(manual_trackdata.xmetri(span,target), manual_trackdata.ymetri(span,target), ...
        manual_trackdata.zmetri(span,target),'r-*');
    set(gca,'YDir','reverse','ZDir','reverse');
    %set(gca,'ZDir','reverse');
    xlabel('asse X');ylabel('asse Y');zlabel('asse Z');
    title(sprintf('Pedone %i - Plot traiettorie', target));

    hold on;
    grid on;
    for ii=1:size(trackdata.xmetri,2)
        indrows=find(trackdata.xmetri(td_span,ii));
        offsets = indrows + td_start - 1;
        plot3(trackdata.xmetri(offsets ,ii), trackdata.ymetri(offsets,ii),...
            trackdata.zmetri(offsets, ii), 'b-o');
        % set(gca,'YDir','reverse','ZDir','reverse');xlabel('asse Y');ylabel('asse X');zlabel('asse Z');
        %pause
    end
    hold off;
    
    looped_once = true;
end
