clear all; close all; clc;

load('SS190\trackdata_SS190_11-02-27.mat');
load('manual-trackdata-SS190.mat');

pedestrians = size(manual_trackdata.I,2);
sequence_len = size(manual_trackdata.I,1);

frame_start = 1;
frame_end = sequence_len;

%% manually tracked pedestrians in the given time_span
manual_trackdata.xmetri(isnan(manual_trackdata.xmetri)) = 0;
[man_curves, man_labels] = make_curve_cell(manual_trackdata, frame_start, frame_end); 

%% automatically tracked pedestrians in the interval closest to time_span
[~,td_start] = min(abs(trackdata.nframe_time(:,1) - frame_start));
[~,td_end] = min(abs(trackdata.nframe_time(:,1) - frame_end));

[auto_curves, auto_labels] = make_curve_cell(trackdata, td_start, td_end);

%% votes
% with each automatically found curve, associate the closest index of the manually tracked curves.
auto_curves_len = length(auto_curves);
votes = NaN(size(auto_curves));
for auto_curve_i = 1:auto_curves_len
    % for each point in each automatically found curve, vote the closest
    % manually found curve
    current_auto_curve = auto_curves{auto_curve_i};
    votes(auto_curve_i) = vote_neighbours(current_auto_curve, man_curves);
end

%% determine the closest curve as mode of votes
% closest(i) is the manually found curve with index
% man_labels(closest(i)) closest to the curve at auto_labels(i), therefore 
% we can say that "auto_label(i) is closer to man_labels(closest(i))", and 
% these numbers are consistent to the columns in the trackdata fields.
closest = NaN(size(votes));
for i=1:auto_curves_len
    closest(i) = votes(i);
end

%% build incidence matrix
% rows = each row is the automatically tracked path indexed at auto_labels,
% i.e. the i-th row corresponds to the trajectory auto_labels(i), according
% to the trackdata column numeration.
% columns = each column is the manually tracked path indexed at
% man_labels. Like above, these indices refer to the manual_trackdata
% columns.
incidence_matrix = false(auto_curves_len, length(man_curves));
for row=1:auto_curves_len
    incidence_matrix(row,closest(row)) = true;
end
