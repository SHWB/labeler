
imrows = size(Zpavimento, 1);
imcols = size(Zpavimento, 2);
[J, I] = meshgrid(1:imcols, 1:imrows);
[xground,yground,zground] = mydepth2realworld(J,I,Zpavimento);

%how many points for LMS regression
samples = 16;
i_rand = randperm(imrows);
i_rand = i_rand(1:samples);
j_rand = randperm(imcols);
j_rand = j_rand(1:samples);

A = [diag(xground(i_rand,j_rand)), diag(yground(i_rand, j_rand)), ones(samples,1)];
b = diag(zground(i_rand, j_rand));

param_xyz = A\b; 

save('param_xyz-SS190.mat','param_xyz');