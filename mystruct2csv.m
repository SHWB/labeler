function mystruct2csv(s,fn)
% STRUCT2CSV(s,fn)
%
% Output a structure to a comma delimited file with column headers
%
%       s : any structure composed of one or more matrices and cell arrays
%      fn : file name
%
%      Given s:
%
%          s.Alpha = { 'First', 'Second';
%                      'Third', 'Fourth'};
%
%          s.Beta  = [[      1,       2;
%                            3,       4]];
% 
%      STRUCT2CSV(s,'any.csv') will produce a file 'any.csv' containing:
%
%         "Alpha",        , "Beta",
%         "First","Second",      1,  2
%         "Third","Fourth",      3,  4
%
% Written by James Slegers, james.slegers_at_gmail.com
% Covered by the BSD License
%

FID = fopen(fn,'w');
headers = fieldnames(s);
m = length(headers);
l = '';
for ii = 1:m
    
    sz(ii,:) = size(getfield(s,headers{ii}));
    l = [l,'"',headers{ii},'",'];
    if strcmp(headers{ii},'pathsequence')
        % non introduco colonne nel file CSV
    else
        if sz(ii,2)>1 
            % per ogni colonna nella matrice che 
            % appartiene al campo analizzato inserisco una colonna nel CSV
            for jj = 2:sz(ii,2)
                l = [l,','];
            end
        end
    end % if strcmp()
end
l = [l,'\n'];
fprintf(FID,l);

n = max(sz(:,1));

for ii = 1:n
    l = '';
    for jj = 1:m
        
        for kk = 1:sz(jj,2)
            
            if strcmp(headers{jj},'pathsequence')
                if (kk==1)
                    c = getfield(s,headers{jj});  
                    str = [c,',']; %GF
                    pos_escape = strfind(str,'\'); % GF sostituisco \ con /
                    str(pos_escape) = '/';
                     l = [l,str];
                     break
                else
                    break
                end % if kk
            elseif sz(jj,1)<ii
                str = [','];
            else
                c = getfield(s,headers{jj});
                % d = size(c); %GF
                if isnumeric(c)
                    str = [num2str(c(ii,kk)),','];
                elseif ischar(c)%GF
                    str = [c,',']; %GF
                    pos_escape = strfind(str,'\'); % GF sostituisco \ con /
                    str(pos_escape) = '/';
                     l = [l,str];
                     break
                else
                    str = ['"',c{ii,kk},'",'];
                end
            end
            l = [l,str];
           
        end
        
    end
    l = [l,'\n'];
    fprintf(FID,l);
    
end
fclose(FID);
