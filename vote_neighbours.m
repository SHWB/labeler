function vote = vote_neighbours(curve, other_curves )
%VOTE_NEIGHBOURS(curve, other_curves) for each point in curve, returns the 
% index of the closest curve in other_curves cell array.
    
    other_curves_len = length(other_curves);
    points_in_curve = size(curve, 1);
    %store distance and closest curve respectively
    votes = zeros(points_in_curve, 2);
    for point_i = 1:points_in_curve
        % find the distance between each point in curve...
        distances_per_point = zeros(other_curves_len, 1);
        for other_curve_i = 1:other_curves_len
            % ...and each curve in other_curves.
            distances_per_point(other_curve_i) = curve_point_distance( ...
                curve(point_i, :), other_curves{other_curve_i});
        end
        %closest_other is the index of the other_curve 
        %which is the closest to the point curve(point_i,:)
        [dist, closest_other] = min(distances_per_point);
        votes(point_i,:) = [dist, closest_other];
    end

    vote = mode(votes(:,2));

end

