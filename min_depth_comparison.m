function MDC = min_depth_comparison(manual_file, DISPLAY)

close all;

load('SS190\trackdata_SS190_11-02-27.mat');
load(manual_file);
load('Zpavimento.mat');

%% compute new xyz using zpavimento depth
pedestrians = size(manual_trackdata.I,2);

%replace zeros with NaNs so that it's possible to find the minima
md_trackdata = trackdata;
md_trackdata.Depth(md_trackdata.Depth == 0) = NaN;
%the max height is the minimum distance from the sensor
%min(zmetri) and min(Depth) give the same results
[amd, ~] = min(md_trackdata.Depth);
Depth = zeros(size(trackdata.Depth));

for col=1:size(trackdata.Depth, 2)
    not_zero_rows = find(trackdata.Depth(:,col));
    if ~isempty(not_zero_rows)
        Depth(not_zero_rows, col) = amd(col);
    end
end

md_trackdata.Depth = Depth;

[md_trackdata.xmetri, md_trackdata.ymetri, md_trackdata.zmetri] = ...
    mydepth2realworld(md_trackdata.J, md_trackdata.I, Depth);

[dis, vel] = compute_velocities(md_trackdata);
md_trackdata.nframe_dxmetri = dis.x;
md_trackdata.nframe_dymetri = dis.y;
md_trackdata.nframe_vxmetri = vel.x;
md_trackdata.nframe_vymetri = vel.y;
md_trackdata.nframe_vxymetri = vel.modulus;

%do the same thing to the Depth field in manual_trackdata
md_man_trackdata = manual_trackdata;
mmd = min(md_man_trackdata.Depth);
Depth = NaN(size(md_man_trackdata.Depth));

for p=1:pedestrians
    not_nan_rows = find(~isnan(manual_trackdata.Depth(:, p))); %must be the same in J
    if ~isempty(not_nan_rows)
        Depth(not_nan_rows,p) = mmd(p);
    end
end
md_man_trackdata.Depth = Depth;

[md_man_trackdata.xmetri, md_man_trackdata.ymetri, md_man_trackdata.zmetri] = ...
    mydepth2realworld(md_man_trackdata.J, md_man_trackdata.I,Depth);

[dis, vel] = compute_velocities(md_man_trackdata);
md_man_trackdata.nframe_dxmetri = dis.x;
md_man_trackdata.nframe_dymetri = dis.y;
md_man_trackdata.nframe_vxmetri = vel.x;
md_man_trackdata.nframe_vymetri = vel.y;
md_man_trackdata.nframe_vxymetri = vel.modulus;


%% show paths
birth = zeros(pedestrians, 1);
life_span = zeros(pedestrians, 1);

for i=1:pedestrians
   not_nans = ~isnan(manual_trackdata.I(:,i)); 
   birth(i) = find(not_nans,1);
   life_span(i) = sum(not_nans);
end

death = birth + life_span - 1;

votes = NaN(pedestrians, 1);
closest = NaN(size(votes));
incidence_matrix = false(pedestrians, size(md_trackdata.xmetri, 2));

looped_once = false;
for target=1:pedestrians
    
    if looped_once
        disp('Press any key to plot the trajectories for the next pedestrian...');
        pause
    end
    
    tot_seq_len = size(manual_trackdata.nframe_time, 1);
    frame_start = birth(target);
    frame_end = death(target);

    span = frame_start:frame_end;

    [~,td_start] = min(abs(trackdata.nframe_time(:,1) - frame_start));
    [~,td_end] = min(abs(trackdata.nframe_time(:,1) - frame_end));
    
    man_curve = [md_man_trackdata.xmetri(span, target), ...
        md_man_trackdata.ymetri(span, target), ...
        md_man_trackdata.zmetri(span, target), ...
        md_man_trackdata.nframe_time(span, 1)];
    
    [md_curves, md_labels] = make_curve_cell(md_trackdata, td_start, td_end);
    
    votes(target) = vote_neighbours(man_curve, md_curves);
    closest(target) = md_labels(votes(target));
    incidence_matrix(target, closest(target)) = true;
    
    MDC.incidence_matrix = incidence_matrix;
    MDC.closest = closest;
    
    td_span = td_start:td_end;
    
    if DISPLAY
        % plot only xmetri and ymetri
        figure(1);
        plot(manual_trackdata.xmetri(span, target), manual_trackdata.ymetri(span, target), 'm-*');
        hold on;
        plot(md_man_trackdata.xmetri(span, target), md_man_trackdata.ymetri(span, target), 'r-*');
        set(gca,'YDir','reverse');
        xlabel('X'); ylabel('Y'); 
        title(sprintf('Pedone %i - Plot X-Y', target));

        grid on; axis equal;
        for ii=1:size(trackdata.xmetri,2)
           %find which rows in the selected time window are not zero for the given
           %pedestrian ii
           indrows=find(trackdata.xmetri(td_span,ii));
           offsets = indrows + td_start - 1;
           %beware that indrows is indexed starting from 1, then it need add the
           %offset
           plot(trackdata.xmetri(offsets, ii), ...
                trackdata.ymetri(offsets, ii),'b-o');
           plot(md_trackdata.xmetri(offsets, ii), ...
               md_trackdata.ymetri(offsets, ii), 'g-o');
        end
        hold off;
        legend('Manual (painted Z)','Manual (min Z)','Auto (painted Z)'...
            ,'Auto (min Z)','Location','southoutside','Orientation','horizontal');

        % plot manual pedestrian
        figure(2);
        plot3(manual_trackdata.xmetri(span,target), manual_trackdata.ymetri(span,target), ...
            manual_trackdata.zmetri(span,target),'m-*');
        hold on;
        plot3(md_man_trackdata.xmetri(span, target), md_man_trackdata.ymetri(span, target), ...
            md_man_trackdata.zmetri(span, target), 'r-*');
        set(gca,'YDir','reverse','ZDir','reverse');
        %set(gca,'ZDir','reverse');
        xlabel('asse X');ylabel('asse Y');zlabel('asse Z');
        title(sprintf('Pedone %i - Plot traiettorie', target));

        grid on; axis equal; axis auto;
        for ii=1:size(trackdata.xmetri,2)
            indrows=find(trackdata.xmetri(td_span,ii));
            offsets = indrows + td_start - 1;
            plot3(trackdata.xmetri(offsets ,ii), trackdata.ymetri(offsets,ii),...
                trackdata.zmetri(offsets,ii), 'b-o');
            plot3(md_trackdata.xmetri(offsets,ii), ...
                md_trackdata.ymetri(offsets, ii), ...
                md_trackdata.zmetri(offsets, ii), 'g-o');
            % set(gca,'YDir','reverse','ZDir','reverse');xlabel('asse Y');ylabel('asse X');zlabel('asse Z');
            %pause
        end
        hold off;
        legend('Manual (painted Z)','Manual (min Z)','Auto (painted Z)'...
            ,'Auto (min Z)','Location','southoutside','Orientation','horizontal');

        looped_once = true;
    end
end
end
