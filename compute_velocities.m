function [displacement, velocity] = compute_velocities( trackdata )
%COMPUTE_VELOCITIES Given trackdata complete with xmetri and ymetri, computes
%speed on each coordinate and shifts.
%   Detailed explanation goes here

    nped = size(trackdata.xmetri, 2);
    displacement = struct;
    velocity = struct;
    
    %fill movement field and stack nans on top to preserve dimensions
    displacement.x = vertcat(NaN(1,nped), diff(trackdata.xmetri));
    displacement.y = vertcat(NaN(1,nped), diff(trackdata.ymetri));
    
    velocity.x = displacement.x ./ ...
        repmat(trackdata.nframe_elapsedtime, [1 nped]);
    velocity.y = displacement.y ./ ...
        repmat(trackdata.nframe_elapsedtime, [1 nped]);
    velocity.modulus = ((velocity.x).^2 + (velocity.y).^2).^(1/2);
end

