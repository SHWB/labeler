function [xm,ym,zm]=mydepth2realworld(x,y,depth)
% https://groups.google.com/forum/#!topic/openkinect/ihfBIY56Is8
constant = 570.3*0.625; 

if ~isa(x,'double') || ~isa(y,'double')
   x = double(x); y = double(y); 
end

fx_d = 1.0 / constant;
fy_d = 1.0 / constant;

cx_d = 160.0;
cy_d = 120.0;

depth=double(depth);
 MM_PER_M = 1000; 
xm = (x - cx_d) .* depth * fx_d/MM_PER_M;
ym = (y - cy_d) .* depth * fy_d/MM_PER_M;
zm = depth/MM_PER_M ;
return 