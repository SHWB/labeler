function Zfiltered = processZ(Z,Zpavimento)
%UNTITLED4 Summary of this function goes here
%   Detailed explanation goes here
    
    Z = medfilt2(double(Z),[5 5]);
    
    %fill the borders
    Z(:,1) = Zpavimento(:,1);
    Z(1,:) = Zpavimento(1,:);
    Z(end,:) = Zpavimento(end,:);
    Z(:,end) = Zpavimento(:,end);
    
    Z(Z==0) = NaN;
    
    Zfiltered = inpaint_nans(Z,2); %immagine riempita

end

