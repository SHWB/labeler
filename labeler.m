function varargout = labeler(varargin)
% LABELER MATLAB code for labeler.fig
%      LABELER, by itself, creates a new labeler or raises the existing
%      singleton*.
%
%      H = LABELER returns the handle to a new labeler or the handle to
%      the existing singleton*.
%
%      LABELER('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in labeler.M with the given input arguments.
%
%      LABELER('Property','Value',...) creates a new labeler or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before labeler_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to labeler_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help labeler

% Last Modified by GUIDE v2.5 31-Mar-2015 17:16:46

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @labeler_OpeningFcn, ...
                   'gui_OutputFcn',  @labeler_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT
end

% --- Executes just before labeler is made visible.
function labeler_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to labeler (see VARARGIN)

% Choose default command line output for labeler
handles.output = hObject;
handles.data = {};

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes labeler wait for user response (see UIRESUME)
% uiwait(handles.figure1);
end

% --- Outputs from this function are returned to the command line.
function varargout = labeler_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;
end

% --------------------------------------------------------------------
function open_menu_Callback(hObject, eventdata, handles)
% hObject    handle to open_menu (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
    folder = uigetdir([],'Select the folder containing the sequence...');
    
    if (folder == 0) %i.e. the user canceled the operation
        return;
    end
    
    %regex that matches the acquired frames
    seqpattern = '.+';
    %seqpattern = 'acq.+[1-9_]+.*';
    
    %load sequence data: these are stored as fields in the handles
    %structure
    handles.openedDir = folder;
    fulldata = loadSequence(folder, seqpattern);
    
    if isempty(fulldata)
        return;
    end
    
    %this regexp matches the opened folder name, i.e. /home/xyz/MATLAB/SSabc
    %returns the /SSabc token. In this mode, it gives the string index where 
    %the match starts
    
    if (ismac || isunix) %mac or unix platform
        starti = regexp(folder, '\/\w+?$');
    else %windows platform
        starti = regexp(folder, '\\\w+?$');
    end
    %upper = folder(1:starti); %the sequence directory's parent directory
    handles.seqID = folder(starti+1:end); %the sequence folder name
    
    handles.NFrames = length(fulldata);
    %the empty data is a 0-by-NFrames empty cell array
    handles.data = cell(0,handles.NFrames);
    handles.fileList = fulldata(:,1);
    
    %populate the frames and time arrays, actually splitting the original
    %raw data structure fields into different appdata, for easier retrieval
    %and lighter memory management.
    %fulldata{i,:} = {path, struct}
    frames = cell(1,handles.NFrames);
    Z_images = cell(1, handles.NFrames);
    times = cell(1,handles.NFrames);
    for i=1:handles.NFrames  
        frames{i} = permute(fulldata{i,2}.rawdata.I, [3,2,1]);
        Z_images{i} = fulldata{i,2}.rawdata.Z';
        times{i} = fulldata{i,2}.rawdata.time;
    end
    
    clear fulldata 
    
    %update some gui elements
    if handles.NFrames > 1
        set(handles.next_button,'Enable','on');
    end
    
    %enable GUI components only when frames have been loaded
    set(handles.frame_tb,'Enable','on');
    set(handles.frame_tb,'String',1);
    set(handles.text_NFrames,'String',['of ', num2str(handles.NFrames)]);
    set(handles.editmode_check,'Enable','on');
    set(handles.load_data_menu,'Enable','on');
    set(handles.export_menu,'Enable','on');
    
    [RGBlines, RGBcolumns, ~] = size(frames{1});
    [Zlines, Zcolumns] = size(Z_images{1});
    
    %attach a cell array containing frames and other struct content to the axes
    setappdata(handles.image_axes,'Frames', frames);
    setappdata(handles.image_axes,'Z', Z_images);
    setappdata(handles.image_axes,'Times',times);
    setappdata(handles.image_axes,'RGBresolution', [RGBlines, RGBcolumns]);
    setappdata(handles.image_axes,'Zresolution', [Zlines, Zcolumns]);
    
    %plot the first frame on image_axes
    handles.image = imshow(frames{1});
    
    %make a context menu to insert points
    insertMenu = uicontextmenu;
    uimenu(insertMenu, 'Label', 'Insert new point...', 'Callback', @insertPoint);
    set(handles.image, 'UIContextMenu', insertMenu);
    
    %set constraint function for image_axes
    setappdata(handles.image_axes,'ConstraintFcn', ...
        makeConstrainToRectFcn(...
            'impoint',get(handles.image_axes,'XLim'),get(handles.image_axes,'YLim')));
        
    %PointsHandles holds the points handles for the current frame
    %setappdata(handles.image, 'PointsHandles', {});
    setappdata(handles.image, 'CurrentFrame', 1);
    
    guidata(hObject, handles);    
    %draw existing points on frame one
    drawPoints(handles.image,1);
    %create callback for clicking on the image
    setFrameHitTest(handles.image,'off');   
    %create callback to intercept keypresses
    set(gcf, 'WindowKeyPressFcn', @WindowKeyPressFcn);
end


% --- Executes on button press in prev_button.
function prev_button_Callback(hObject, eventdata, handles)
% hObject    handle to prev_button (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
    nextFrame = getappdata(handles.image,'CurrentFrame');
    destinationFrame = nextFrame - 1;
    
    %this check is evaluated only when the callback is called by window keypress
    %listener
    if destinationFrame == 0
        return;
    end
    
    iseditmode = get(handles.editmode_check, 'Value');
    
    set(handles.next_button,'Enable','on');
    set(handles.frame_tb,'String',num2str(destinationFrame));
    
    if destinationFrame == 1
        set(hObject,'Enable','off');
    end
    
    %clear the points on the current frame
    clearPoints(handles.image);
    
    %update the frame and load the related frame
    setappdata(handles.image, 'CurrentFrame', destinationFrame);
    
    frames = getappdata(handles.image_axes,'Frames');
    set(handles.image,'CData',frames{destinationFrame});
    %don't overload memory
    clear frames
    %if edit mode is on, forget points in the future
    if iseditmode
        forget(hObject, destinationFrame);
    end
    %draw the points in the previous frame
    drawPoints(handles.image, destinationFrame);
    
end

% --- Executes on button press in next_button.
function next_button_Callback(hObject, eventdata, handles)
% hObject    handle to next_button (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

%When the next button is pressed the current frame is updated. Furthermore
%if the points in the previous image were moved, their position is updated
%in the data array by updatePoints. Finally the frame is updated WITHOUT
%plotting the image again, but changing its CData attribute. In this way,
%there's no need to redraw the points since they're left on the canvas.
    
    prevFrame = getappdata(handles.image,'CurrentFrame');
    destinationFrame = prevFrame + 1;
    %this check is evaluated only when the callback is called by window keypress
    %listener
    if destinationFrame > handles.NFrames
        return;
    end

    set(handles.prev_button,'Enable','on');
    set(handles.frame_tb,'String',num2str(destinationFrame));
    
    if destinationFrame == handles.NFrames
        set(hObject, 'Enable', 'off');
    end
       
    if get(handles.editmode_check,'Value') == 1
        %If we're in edit mode points are inherited from the previous frame
        %When points are inherited graphically, they have to be copied
        %into the data cell array. This avoids bugs when the user doesn't move
        %anything in a frame.
        handles.data(:, destinationFrame) = handles.data(:, prevFrame);
        guidata(hObject, handles);
    else
        %When edit mode is off, behave like normally prev_button does:
        %clear all points and redraw from the array handles.data
        clearPoints(handles.image);
        drawPoints(handles.image, destinationFrame);
    end 
    
    %update frame, this is done regardless of the edit mode
    setappdata(handles.image, 'CurrentFrame', destinationFrame);
    frames = getappdata(handles.image_axes,'Frames');    
    set(handles.image,'CData',frames{destinationFrame});
    %no need to clear frames here since the workspace is cleared after the
    %function returns.
end

function frame_tb_Callback(hObject, eventdata, handles)
% hObject    handle to frame_tb (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of frame_tb as text
%        str2double(get(hObject,'String')) returns contents of frame_tb as a double

    targFrame = round(str2double(get(gcbo,'String')));
    if targFrame <= handles.NFrames && targFrame > 0
                
        frames = getappdata(handles.image_axes,'Frames');
        
        %destroy all the points
        clearPoints(handles.image);
        
        %set the visualization data
        set(handles.frame_tb,'String', num2str(targFrame));
        set(handles.image,'CData', frames{targFrame});
        
        %don't overload memory
        clear frames
               
        drawPoints(handles.image,targFrame);
        
        %if we moved to the last frame, disable next_button
        if targFrame == handles.NFrames
            set(handles.next_button, 'Enable', 'off');
        else
            set(handles.next_button, 'Enable', 'on');
        end
        
        %if we moved to the first frame, disable prev_button
        if targFrame == 1
            set(handles.prev_button,'Enable','off');
        else
            set(handles.prev_button,'Enable','on');
        end
        
        setappdata(handles.image, 'CurrentFrame', targFrame);
    else
        e = errordlg('Frame doesn''t exist','ERROR','modal');
        uiwait(e);
        currentFrame = getappdata(handles.image,'CurrentFrame');
        set(handles.frame_tb, 'String', num2str(currentFrame));
    end
end

% --- Executes on button press in editmode_check.
function editmode_check_Callback(hObject, eventdata, handles)
% hObject    handle to editmode_check (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of editmode_check

%The edit mode toggles whether the user can add points
    
    iseditmode = get(handles.editmode_check, 'Value');

    if iseditmode
                    
        message = ['Switching to Edit Mode will discard all '...
            'the points stored after the current frame'];
        question = 'Do you want to enter Edit Mode?';

        answer = questdlg({message, question}, 'Warning','Yes','No','Yes');
        
        switch answer
            case 'Yes'
                forget(hObject, getappdata(handles.image, 'CurrentFrame'));
                setFrameHitTest(handles.image, 'on');
                set(handles.frame_tb, 'Enable', 'off');
            case {'No', ''}
                set(hObject, 'Value', 0);
        end
        
    else %edit mode is disabled
        setFrameHitTest(handles.image, 'off');  
        set(handles.frame_tb, 'Enable', 'on');
    end

end

% --------------------------------------------------------------------
function File_menu_Callback(hObject, eventdata, handles)
% hObject    handle to File_menu (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
end

function drawPoints(imageHandle, frameNo)
%DRAWPOINTS(imageHandle, frame) draws the points stored in
%handles.data onto the frame frameNo
%   imageHandle    is the handle to the image where the function draws on
%   frameNo        is the frame number

    handles = guidata(imageHandle);
    points = handles.data(:, frameNo);
    nonempty_idx = find(~cellfun(@isempty, points));
    hpoints = cell(length(nonempty_idx), 1);
    cfcn = getappdata(handles.image_axes,'ConstraintFcn');
    
    for i=1:length(nonempty_idx)
        hpoint = impoint(handles.image_axes, handles.data{nonempty_idx(i),frameNo},...
                'PositionConstraintFcn', cfcn);
        %every time a valid point is drawn onto the image, store it
        %into the hpoints cell array.
        hpoints{i} = hpoint;  
        %set appeareance, label and callbacks
        setPointProperties(hpoint, nonempty_idx(i)); 
    end

    setappdata(imageHandle,'PointsHandles', hpoints);
end

function insertPoint(hObject, eventdata)
    handles = guidata(hObject);
    im_axes = handles.image_axes;
    image = handles.image;
    frame = getappdata(image, 'CurrentFrame');
    coordinates = get(gca,'CurrentPoint');
    i = coordinates(2,2);
    j = coordinates(2,1);
    hpoint = impoint(gca,[j i],'PositionConstraintFcn', ...
        getappdata(im_axes, 'ConstraintFcn'));
    
    defaultAns = {num2str(length(handles.data(:, frame)) + 1)};
    answer = inputdlg({'Label (integer):'}, 'Insert Label', 1, defaultAns);
    if isempty(answer) %has the Cancel button been pressed?
        delete(hpoint);
    else
        answer = str2double(answer); %str2double returns NaN if parsing fails
        if ~isnan(answer) %was the entry a number?
            answer = round(answer);
            hpoints = getappdata(image, 'PointsHandles');
            %put the coordinates in data{answer,frame}
            handles.data{answer, frame} = [j, i];
            %keep track of the handles in an impoint handle array
            hpoints{answer} = hpoint;
            setPointProperties(hpoint, answer);
            setappdata(image, 'PointsHandles', hpoints);
        else
            e = errordlg('Invalid input format','ERROR','modal');
            uiwait(e);
            delete(hpoint);
        end
    end
    guidata(hObject, handles);
end

function setPointProperties(hpoint, index)
    setColor(hpoint,'r');
    
    %set and style the label
    setString(hpoint,num2str(index));
    hpchildren = get(hpoint,'Children');
    %hpchildren(1) is the label
    %hpchildren(2) and (3) control point appearance
    set(hpchildren(1), 'FontSize', 15); 
    set(hpchildren(3), 'MarkerFaceColor',[1 0 0]);
    set(hpchildren(2), 'Color', [1 0 0], 'LineWidth', 0.85, 'MarkerSize', 20);  
    % replacing point context menu
    newmenu = uicontextmenu;
    uimenu(newmenu,'Label','Delete','Callback',{@deleteExistingPoint, hpoint});
    set(hpoint,'UIContextMenu',newmenu);
    
    %adding callback for moving action:
    %we first need to define an anonymous function for currying updatePoint,
    %since the fcn callback in addNewPositionCallback passes ONLY the
    %current point's position and there's no way to know which point has
    %been modified.
    curryed_updatePoint = @(pos) updatePoint(pos, hpoint);
    addNewPositionCallback(hpoint, curryed_updatePoint);    
end

% --- Executes during object creation, after setting all properties.
function frame_tb_CreateFcn(hObject, eventdata, handles)
% hObject    handle to frame_tb (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
    if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
        set(hObject,'BackgroundColor','white');
    end
end

function updatePoint(position, hpoint)
%this is called every time the point is dragged around. Only updates the
%point in data according to the label
    ax = get(hpoint,'Parent');
    handles = guidata(ax);
    hpchildren = get(hpoint,'Children');
    numlabel = round(str2double(get(hpchildren(1),'String')));
    currFrame = getappdata(handles.image, 'CurrentFrame');
    handles.data{numlabel, currFrame} = position;
    guidata(ax, handles);
end

function deleteExistingPoint(hObject, eventdata, point)
%DELETEEXISTINGPOINT deletes a point POINT both from the image and both
%from the image canvas and the handles.data cell array
    handles = guidata(hObject);
    hpchildren = get(point, 'Children');
    image = handles.image;
    %remember hpchildren(1) is the label
    index = round(str2double(get(hpchildren(1), 'String')));
    %this index is used to retrieve the points coordinates in the
    %handles.data cell array
    currFrame = getappdata(image, 'CurrentFrame');
    hpoints = getappdata(image, 'PointsHandles');
    handles.data{index, currFrame} = [];
    delete(point);
    %overwrite hpoints{index} handle, which would be to a deleted object 
    hpoints{index} = [];
    setappdata(image,'PointsHandles', hpoints);
    %call guidata because edit on data
    guidata(hObject, handles);
end

function clearPoints(imageHandle)
%CLEARPOINTS clears all the points drawn on the current image referred by
%IMAGEHANDLE. It updates the handles list concordantly
    if isappdata(imageHandle,'PointsHandles')
        hpoints = getappdata(imageHandle,'PointsHandles');
    else
        hpoints = {};
    end
    for i=1:length(hpoints)
        point = hpoints{i};
        if isempty(point)
           continue; 
        end
        delete(point);
        hpoints{i} = [];
    end
    setappdata(imageHandle,'PointsHandles',hpoints);   
end

function deletePoints(imageHandle)
    hpoints = getappdata(imageHandle,'PointsHandles');
    for i=1:length(hpoints)
        point = hpoints{i};
        if isempty(point)
           continue; 
        end
        deleteExistingPoint(imageHandle,[],point);
    end
    setappdata(imageHandle,'PointsHandles',hpoints); 
end

function forget(hObject ,index)
%FORGET clears all the coordinates in handles.array after the frame
%at index
    handles = guidata(hObject);
    coordRows = length(handles.data(:,index));
    %clear all the coordinates AFTER index
    handles.data(:, index+1:end) = cell(coordRows, handles.NFrames - index);
    guidata(hObject, handles);
end

function setFrameHitTest(imageHandle, status)
%SETFRAMEHITTEST controls the creation, moving and the deletion of each point on
%the image. This is done by setting on/off the HitTest property on the image
%(controls creation) and on the points themselves (controls dragging).
%Disabling the HitTest on the image also prevents the context menu to pop,
%thus not allowing the user to delete the points.

    hpoints = getappdata(imageHandle,'PointsHandles');
    set(imageHandle,'HitTest', status);
    
    for i=1:length(hpoints)
       set(hpoints{i},'HitTest', status);
    end
end

% --------------------------------------------------------------------
function save_menu_Callback(hObject, eventdata, handles)
% hObject    handle to save_menu (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

    default = ['labels-data-', handles.seqID, '.mat'];
    [file, path] = uiputfile('*.mat','Save Points Data...', default);
    
    if isnumeric(file)
        return;
    end
    data = handles.data;
    save([path file],'data');
    disp([path file ' has been saved.']);
end


% --------------------------------------------------------------------
function load_data_menu_Callback(hObject, eventdata, handles)
% hObject    handle to load_data_menu (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
    
    [filename, path, ~] = uigetfile('*.mat','Select the labels file');
    
    if path == 0
        return;
    end
    
    load([path filename]);
    
    if exist('data', 'var')
        if (size(data, 2) == handles.NFrames)
            iseditmode = get(handles.editmode_check,'Value');
            handles.data = data;
            guidata(hObject, handles);
            image = handles.image;
            clearPoints(image);
            drawPoints(image, getappdata(image, 'CurrentFrame'));
            if iseditmode 
                setFrameHitTest(image, 'on');
            else
                setFrameHitTest(image, 'off');
            end 
        else
            warndlg(['The data you are trying to import have a different size'....
                'than the frame sequence loaded.'], 'Data dimensions mismatch',...
                'modal');
        end
    else
        warndlg('The file loaded does not contain any entry of data', ...
            'Content error', 'modal');
    end
end

% --------------------------------------------------------------------
function export_menu_Callback(hObject, eventdata, handles)
% hObject    handle to export_menu (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
end

% --------------------------------------------------------------------
function export_csv_Callback(hObject, eventdata, handles)
% hObject    handle to export_csv (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
    
    
    if (ismac || isunix) %mac or unix platform
        default_fn = [pwd,'/manual-trackdata-',handles.seqID];
    else %windows platform
        default_fn = [pwd,'\manual-trackdata-',handles.seqID];
    end
    
    [mfile, mpath] = uiputfile('.mat','Export the trackdata structure...', ...
        [default_fn '.mat']);
    [cfile, cpath] = uiputfile('.csv','Export the trackdata csv...', ...
        [default_fn '.csv']);
    
    %cfile and mfile are 0 if the user clicked the cancel button
    if isnumeric([cfile mfile])
        return;
    end
    
    load('Zpavimento.mat');
    
    if ~exist('Zpavimento','var')
       warndlg('Make sure Zpavimento.mat contains valid data.', ...
           'Data not found', 'modal');
       return;
    end
    
    ZPres = size(Zpavimento);
    Zres = getappdata(handles.image_axes,'Zresolution');
    
    if ~isequal(ZPres,Zres)
       warndlg('The file you loaded doesn''t have the same resolution as the acquired data',...
           'Resolution Mismatch', 'modal');
       return;
    end
    
    wbh = waitbar(0, 'Repainting depth frames');
    
    Z_raw = getappdata(handles.image_axes,'Z');
    Z_filled = cell(size(Z_raw));
    frames = handles.NFrames;
    for i=1:frames
        Z_filled{i} = processZ(Z_raw{i}, Zpavimento);
        waitbar(i*0.5/frames, wbh);
    end
    
    % this may come in handy if some evaluations are needed
    % save Z_filled
    
    clear Z_raw
    
    progress = 0.5; %length of waitbar
    delta = 0.05; %define 10 steps from 50 to 100%
    waitbar(progress,wbh,'Computing trackdata fields');
    %the new structure is row oriented, therefore a transposition is needed on
    %the original data.
    JIcell = handles.data';
    dim = size(JIcell);
    nframes = handles.NFrames;
    nped = dim(2);
    
    %initializations
    manual_trackdata = struct;
    I = NaN(dim);
    J = NaN(dim);
    Depth = NaN(dim);
    label = NaN(dim);
   
    manual_trackdata.Area = NaN;
    
    %creates I and J matrices filled with coordinates taken from the JI cell
    %array.
    for i=1:nped
        [J(:,i), I(:,i)] = splitcolumn(JIcell(:,i));
    end
    
    %it may happen that the RGB channel has a different resolution than the 
    %Depth channel. It's necessary to compute scaling coefficients to match the
    %Depth resolution.
    RGBres = getappdata(handles.image_axes,'RGBresolution');
    rgb2zscale = Zres./RGBres;
    I = round(rgb2zscale(1)*I);
    J = round(rgb2zscale(2)*J);
    manual_trackdata.I = I;
    manual_trackdata.J = J;
    update_bar(wbh, progress, delta);
    
    %fill the Depth matrix with the values at the non-nan coordinates in
    %I and J
    for f=1:nframes
        not_nan_indices = find(~isnan(I(f,:))); %must be the same in J
        if ~isempty(not_nan_indices)
            for i=not_nan_indices
                Depth(f,i) = Z_filled{f}(I(f,i), J(f,i));
            end
        end
    end
    manual_trackdata.Depth = Depth;
    update_bar(wbh, progress, delta);
    
    %fill the nframe_time fields
    time_cell = getappdata(handles.image_axes, 'Times');
    manual_trackdata.nframe_time = [(1:nframes)', cell2mat(time_cell')];
    
    %fill the x,y,z fields
    [manual_trackdata.xmetri, manual_trackdata.ymetri, manual_trackdata.zmetri] = ...
        mydepth2realworld(J,I,Depth);
    update_bar(wbh, progress, delta);
    
    %these are set to nan because they've no use in the manual labeled
    %structure
    manual_trackdata.label = label;
   
    manual_trackdata.pathsequence = handles.openedDir;
    
    %use the resolution from Zpavimento because it's supposed to be the
    %same of the frames
    manual_trackdata.rectangle = [1;1;ZPres(1);ZPres(2)]; %image shape
    
    manual_trackdata.nframe_elapsedtime = frame_by_frame_etime(manual_trackdata.nframe_time);
    update_bar(wbh, progress, delta);

    
    [displacement, vel] = compute_velocities(manual_trackdata);
    manual_trackdata.nframe_dxmetri = displacement.x;
    manual_trackdata.nframe_dymetri = displacement.y;
    manual_trackdata.nframe_vxmetri = vel.x;
    manual_trackdata.nframe_vymetri = vel.y;
    manual_trackdata.nframe_vxymetri = vel.modulus;
    update_bar(wbh, progress, delta);
    
    if ischar(mfile)
        waitbar(0.75,wbh,'Saving .mat file');
        save([mpath mfile], 'manual_trackdata');
        disp([mpath mfile ' has been saved.']);
    end
    
    if ischar(cfile)
        waitbar(0.8,wbh, 'Exporting CSV');
        mystruct2csv(manual_trackdata, [cpath cfile]);
        disp([cpath cfile ' has been exported.']);
    end
    
    waitbar(1,wbh);
    delete(wbh);
end

function WindowKeyPressFcn(hObject, eventdata)
% hObject    handle to figure1 (see GCBO)
% eventdata  structure with the following fields (see FIGURE)
%	Key: name of the key that was pressed, in lower case
%	Character: character interpretation of the key(s) that was pressed
%	Modifier: name(s) of the modifier key(s) (i.e., control, shift) pressed
    handles = guidata(hObject);
    switch eventdata.Key
        case {'a', 'leftarrow'}
            prev_button_Callback(handles.prev_button, eventdata, handles);
        case {'d', 'rightarrow'}
            next_button_Callback(handles.next_button, eventdata, handles);
    end
end

function update_bar(wbh, progress, delta) 
        progress = progress + delta; 
        waitbar(progress,wbh);
end

function [c1, c2] = splitcolumn(cellcol)
%instead of a data column holding [x y] arrays, creates 2 double-typed 
%arrays holding x and y.
    F = size(cellcol);
    c1 = NaN(F);
    c2 = NaN(F);
    for i=1:length(cellcol)
        point = cellcol{i};
        if ~isempty(point);
            c1(i) = point(1);
            c2(i) = point(2);
        end
    end
end

function etime_vector = frame_by_frame_etime(nframe_time_matrix)
% determina per ogni frame il tempo (in secondi) trascorso rispetto al
% frame precedente
    nrow = size(nframe_time_matrix,1);
    etime_vector = NaN(nrow,1); % inizializzo il vettore
    if (nrow > 1) % se ci fosse solo un frame, non calcolo tempo tra frame
        for t_i =2:nrow
            t_frame_back = nframe_time_matrix(t_i - 1, 2:7);
            t_curr_frame = nframe_time_matrix(t_i, 2:7);
            etime_vector(t_i) = etime(t_curr_frame, t_frame_back);
        end 
    end 
end
